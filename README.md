![My guiding principle](https://imgs.xkcd.com/comics/the_difference.png)
# About
I have twice gotten extremely nauseous during a workout. I want to address the issue intentionally and scientifically.

# Goal
Do a 1 hour cardio class without feeling nauseous. If that is achieved, condense and share the knowledge with others.

# Procedure
I'm going to estimate the probability of success with a variety of approaches, and iteratively focus in on the ones that seem to be working

# Results
  * __Sugar__: _Very_ small RCT gave a p value of 0.3, and a quick and dirty bayesian analysis gave a ~68% chance of some effect
    * A problem is that even gathering these 14 data points was extremely difficult, so gathering the 2-8x that I'd need to draw a firmer conclusion feels more 'expensive' in terms of time and energy than I can justify
  * __Anti-Reflux Medication__: Might reduce my chances of feeling nauseous by between 25-50%, according to 2015.11.03 to 2016.04.24, though symptoms have been entirely managable without it at various times
    * Importantly, I took it during a relatively short and specific period, so other factors might be confoudning
    * A second analysis, 2017.04.26 to 2017.08.08, found no clear benefit to Omeprazole
  * __Coffee__: Might increase my chances of feeling nauseous by about 50%, according to 2015.11.03 to 2016.04.24. However, cutting it out has not eliminated symptoms, so I'm still drinking it
  * __Dietary Fat__: An analysis conducted 2017.04.26 to 2017.08.08, found a relationship between fat intake and no-nausea mornings (p=.009). 
    * The 2016 analysis found no such relationship, but was using a very crude "this food feels fatty" count, as opposed to the dietary tracking app that I used in 2017
    * Surprisingly, neither analysis found a relationship between fat intake and feeling especially _bad_, so the cause of occasional spikes in nausea are still unclear
  * __Tums__: I was surprised to remember that I've mostly been doing this since the 2017 analysis, so I don't have stats that it works. But the effect is so quick that I'm fairly highly confident. However, it's just a band-aid solution
  * __Auto-correlation__: Given my assumption that impacts are long lasting, there's a surprisingly small correlation between outcomes on consecutive days. This provides some support for the idea that each day can be handled as an independent test
  

# Conclusions
There's a bunch that I don't know. Avoiding fat in general seems to be a generally good move, but I still get flare ups that I can't explain. Broad self-tracking doesn't seem to be yielding answers, but randomized controlled trials are really difficult. There's definitely a lot of careful science still to be done.
